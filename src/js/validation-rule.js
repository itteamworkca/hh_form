(function($){
    $.fn.validationEngineLanguage = function(){};
    $.validationEngineLanguage = {
        newLang: function(){
            $.validationEngineLanguage.allRules = {
                "required": {
                    "regex": "none",
                    "alertText": "* Este campo é obrigatório",
                    "alertTextCheckboxMultiple": "* Favor selecionar uma opção",
                    "alertTextCheckboxe": "* Este checkbox é obrigatório",
                    "alertTextDateRange": "* Ambas as datas do intervalo são obrigatórias"
                },
                "empty": {
                    "func": function(field, rules, i, options){
                        var sts = field.val().length==0;
                        return !sts;
                    },
                    "alertText": "* Este campo não pode ser vazio"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* Favor selecionar ao menos ",
                    "alertText2": "opção"
                },
                "phone": {
                    "regex": /^\([1-9]{2}\) [0-9]{4}\-[0-9]{4}$/,
                    "alertText": "* Número de telefone inválido"
                },
                "celphone": {
                    "regex": /^\([1-9]{2}\) [2-9][0-9]{3,4}\-[0-9]{4}$/,
                    "alertText": "* Número de celular inválido"
                },
                "email": {
                    "regex": /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
                    "alertText": "* Endereço de email inválido"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* Número inteiro inválido"
                },
                "number": {
                    "regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    "alertText": "* Número decimal inválido"
                },
                "date": {
                    "regex": /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/,
                    "alertText": "* Data inválida."
                },
                "time": {
                    "regex": /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/,
                    "alertText": "* Horário inválido. O formato deve ser HH:MM e respeitar minutos e segundos."
                },
                "hour": {
                    "regex": /^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/,
                    "alertText": "* Tempo inválido."
                },
                "onlyNumberSp": {
                    "regex": /^[0-9\ ]+$/,
                    "alertText": "* Apenas números"
                },
                "onlyLetterSp": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* Apenas letras"
                },
                "onlyLetterAccentSp":{
                    "regex": /^[a-z\u00C0-\u017F\ ]+$/i,
                    "alertText": "* Apenas letras e espaços."
                },
                "onlyLetterNumber": {
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "* Não são permitidos caracteres especiais"
                },
                "rg": {
                    "regex": /^[0-9a-zA-Z-.]+$/,
                    "alertText": "* RG inválido"
                },
                "address": {
                    "regex": /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ -]+$/i,
                    "alertText": "* Não são permitidos caracteres especiais e números"
                },
                "real": {
                    // Brazilian (Real - R$) money format
                    "regex": /^([1-9]{1}[\d]{0,2}(\.[\d]{3})*(\,[\d]{0,2})?|[1-9]{1}[\d]{0,}(\,[\d]{0,2})?|0(\,[\d]{0,2})?|(\,[\d]{1,2})?)$/,
                    "alertText": "* Número decimal inválido"
                },
                "cpf": {
                    // CPF is the Brazilian ID
                    "func": function(field, rules, i, options){
                        cpf = field.val().replace(/[^0-9]+/g, '');
                        while(cpf.length < 11) cpf = "0"+ cpf;

                        var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
                        var a = cpf.split('');
                        var b = new Number;
                        var c = 11;
                        b += (a[9] * --c);
                        if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
                        b = 0;
                        c = 11;
                        for (y=0; y<10; y++) b += (a[y] * c--);
                        if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

                        var error = false;
                        if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) error = true;
                        return !error;
                    },
                    "alertText": "CPF inválido",
                    "alertTextOK": "CPF válido"
                },
                "requiredInFunction": {
                    "func": function(field, rules, i, options){
                        return (field.val() == "test") ? true : false;
                    },
                    "alertText": "* Os campos devem conter 'test'"
                }
            };

        }
    };

    $.validationEngineLanguage.newLang();

})(jQuery);
