'use strict';

angular.module('ciebform').controller('FormularioController', FormularioController)

function FormularioController(API, CEP, FILE_UPLOAD, $http, $log, $scope, FileUploader, $timeout, $anchorScroll) {
    var logger = $log.getInstance('FormularioController');

    logger.trace('API: ', API);
    logger.trace('$scope: ', $scope);

    $scope.entity = {};
    $scope.entity['empreendedor'] = {};
    $scope.entity['empreendedor']['toggleListInsert'] = [true];

    $scope.currentStep = 1;
    $scope.listaEmpreendedores = [];
    $scope.listaClassificacoes = [];
    $scope.listaClientes = [];
    $scope.clientesProduto = [];
    $scope.imagensProduto = [];
    $scope.documentosEstudoDeCaso = [];
    $scope.selectedTagId = {};

    $scope.documentUploader = new FileUploader(angular.extend(API.UPLOAD(), {name: 'document'}));
    $scope.documentUploader.filters.push(FILE_UPLOAD.DOCUMENT_FILTER);
    $scope.documentUploader.onWhenAddingFileFailed = onWhenAddingFileFailed;
    $scope.documentUploader.onSuccessItem = onSuccessItem;
    $scope.documentUploader.onErrorItem = onErrorItem;

    $scope.imageUploader = new FileUploader(angular.extend(API.UPLOAD(), {name: 'image'}));
    $scope.imageUploader.filters.push(FILE_UPLOAD.IMAGE_FILTER);
    $scope.imageUploader.onWhenAddingFileFailed = onWhenAddingFileFailed;
    $scope.imageUploader.onSuccessItem = onSuccessItem;
    $scope.imageUploader.onErrorItem = onErrorItem;

    $scope.Typeahead = function(options) {
        if (!Bloodhound) {
            return false;
        }

        var autocomplete = options.element;

        var typeahead = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace(options.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: options.limit || 20,
            remote: {
                cache: false,
                url: options.endpoint,
                wildcard: '%QUERY',
                rateLimitWait: 700
            }
        })

        typeahead.initialize();

        autocomplete.typeahead({
            hint: options.hint || true,
            highlight: options.highlight || true,
            minLength: options.minLength || 3,
        }, {
            display: options.value,
            source: typeahead.ttAdapter(),
            templates: options.template
        }).bind('typeahead:selected', function(obj, datum) {
            options.callback(obj, datum);
        }).bind('typeahead:autocompleted', function(obj, datum) {
            options.callback(obj, datum);
        }).on('keyup keypress', function(event) {
            var code = event.keyCode || event.which,
                value = autocomplete.val();

            if (code === 13) {
                event.preventDefault();
                return false;
            }
        }).on('click', function(e) {
            if (typeof e.srcElement != "undefined" && e.srcElement.value != '') {
                return false;
            }
        }).focusout(function(e){
            if(e.target.value==''){
                autocomplete.typeahead('val', '');
            }
        });
    };

    function validateEngine(){
        $('form.form-validation').validationEngine('detach').validationEngine('attach', {
            showArrowOnRadioAndCheckbox: true,
            promptPosition: "bottomLeft",
            autoHidePrompt:true,
            autoPositionUpdate:true,
            autoHideDelay: 5000,
            validateNonVisibleFields: false,
            maxErrorsPerField: 1,
            focusFirstField: true,
            showOneMessage: true
        })
    }

    function maskEngine(){
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.br_phone').mask('(00) 0000-0000');
        $('.br_celphone').mask('(00) 0000#-0000');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
        $('.number').mask("#0", {reverse: true});
    }

    $scope.startTagsTypeahead = function(){
        var element = $('[name="produto.tags.nome"]');
        logger.debug('Typeahead: ', element);

        $timeout(function(){
            logger.debug('Typeahead 1000: ', element);

            $scope.Typeahead({
                element: element,
                endpoint: API.TAG().url + '%QUERY',
                value: function(data) {
                    return data.nome;
                },
                template: {
                    empty: '<p><i class="fa fa-exclamation-circle m-r-5"></i>Nenhum registro encontrado</p>',
                    loading: "<p>Buscando...</p>",
                    suggestion: function(data) {
                        return '<p>' + data.nome + '</p>'
                    }
                },
                callback: function(obj, data) {
                    angular.element(obj.target).controller("ngModel").$setViewValue(data.nome);
                    $scope.selectedTagId[data.nome] = data.id;
                }
            });
        }, 1000);
    }


    $scope.onRemove = function(item, itens) {
        logger.debug('onRemoveItem', item, itens);
        item.remove();

        itens = itens.filter(function(file) {
            return file != item.file.entity.id;
        });

        // TODO requisitar remoção de arquivo
    }

    $scope.init = function() {
        $http.get('json/form.json').then(function(res) {
            $scope.formItems = res.data;

            validateEngine();

            $http(API.FILTROS()).then(function(response) {
                logger.trace('Filtros', response.data);
                $scope.filtros = response.data;
            });
        });

    }


    $scope.$on('firstRepeat', function(){
        logger.debug('firstRepeat');
        validateEngine();
        maskEngine();
        $scope.$apply();
    });

    $scope.$on('middleRepeat', function(){
        logger.debug('middleRepeat');
        validateEngine();
        maskEngine();
        $scope.entity['empreendedor']['toggleListInsert'] = $scope.listaEmpreendedores.length ? [false] : [true];
        $scope.$apply();
    });

    $scope.$on('lastRepeat', function(){
       logger.debug('lastRepeat');
       $scope.$apply(function () {
           setTimeout(function(){ $scope.startTagsTypeahead(); }, 1000);
           validateEngine();
           maskEngine();
       })
    });

    function showPromptError(elem, msg){
        elem.validationEngine('showPrompt', msg, 'error', 'bottomLeft', true);
        $('html, body').animate({ scrollTop: elem.offset().top - 100 }, 400);
        setTimeout(function(){ elem.validationEngine('hideAll') }, 5000);
    }

    function checkNotEmpty(elem){
        return typeof elem.val() == "undefined" ? true : elem && typeof elem != "undefined" && elem.val().length;
    }

    $scope.toggleView = function(view){
        view = view.split('.');
        return $scope.entity[view[0]][view[1]][0];
    }

    $scope.stepnav = function(step){
        if(!$('form[name="form_1"]').validationEngine('validate'))
                return false

        $scope.currentStep = step;

    }

    $scope.nextStep = function(){
        if(!$('form[name="form_1"]').validationEngine('validate')){
                return false
        }

        if($('form[name="form_2"]').length && !$scope.listaEmpreendedores.length){
                showPromptError($('.listaEmpreendedores'), 'Cadastre no mínimo 1 empreendedor');
                return false
        }

        $scope.currentStep++;
        $anchorScroll();
    }

    $scope.prevStep = function(){
        $scope.currentStep--;
        $anchorScroll();
    }

    $scope.getModel = function(path) {
      var segs = path.split('.');
      var root = $scope.entity;

      while (segs.length > 0) {
        var pathStep = segs.shift();
        if (typeof root[pathStep] === 'undefined') {
          root[pathStep] = segs.length === 0 ? [ '' ] : {}
        }
        root = root[pathStep];
      }
      return root;
    }

    function getProps(obj){
      var objToExport = {};
      for (var property in obj) {
        if (obj.hasOwnProperty(property)){
            if (obj[property].constructor == Object) {
                objToExport[property] = getProps(obj[property]);
            } else {
                if(obj[property][0]&&obj[property][0]!=''&&obj[property][0]!=null&& typeof obj[property][0].value === 'boolean') {
                    objToExport[property] = obj[property][0].value;
                } else if(obj[property][0]&&obj[property][0]!=''&&obj[property][0]!=null&&obj[property][0].value){
                    objToExport[property] = obj[property][0].value;
                } else {
                    objToExport[property] = obj[property][0]&&obj[property][0]!=''&&obj[property][0]!=null ? obj[property][0] : null;
                }
            }
        }
      }
      return objToExport;
    }

    $scope.formButtonClick = function(action){
        switch(action) {
            case "cadastrarEmpreendedor":
                $scope.cadastrarEmpreendedor();
                break;
        }
    }

    $scope.cadastrarEmpreendedor = function(){
        if(!$('form[name="form_2"]').validationEngine('validate'))
            return false

        var emps = angular.copy($scope.entity.empreendedor)
        var empsF = getProps(emps);
        $scope.listaEmpreendedores.push(empsF);
        for (var k in $scope.entity.empreendedor) {
            if ({}.hasOwnProperty.call($scope.entity.empreendedor, k)) {
                $scope.entity.empreendedor[k][0]='';
            }
        }
        for (var i in $scope.entity.empreendedor.endereco) {
            if ({}.hasOwnProperty.call($scope.entity.empreendedor.endereco, i)) {
                if ($scope.entity.empreendedor.endereco[i].constructor == Array) {
                    $scope.entity.empreendedor.endereco[i][0]='';
                }
            }
        }
        $anchorScroll();
        $scope.entity['empreendedor']['toggleListInsert'] = [false];
    }

    $scope.cadastrarClassificacao = function(){
        if( !checkNotEmpty( $('.validateStep3Classificacao.ng-empty:visible').eq(0) )){
            showPromptError($('.validateStep3Classificacao.ng-empty:visible').eq(0), 'Este campo é obrigatório');
            return
        }

        var clas = angular.copy($scope.entity.produto.tags)
        var clasF = getProps(clas);
        clasF['id'] = $scope.selectedTagId[clasF['nome']];
        $scope.listaClassificacoes.push(clasF);
        for (var k in $scope.entity.produto.tags) {
            if ({}.hasOwnProperty.call($scope.entity.produto.tags, k)) {
                $scope.entity.produto.tags[k][0]='';
            }
        }
    }

    $scope.cadastrarCliente = function(){
        if( !checkNotEmpty( $('.validateStep1Clientes.ng-empty:visible').eq(0) )){
            showPromptError($('.validateStep1Clientes.ng-empty:visible').eq(0), 'Este campo é obrigatório');
            return
        }

        var cli = angular.copy($scope.entity.empresa.principaisClientes)
        var cliF = getProps(cli);
        $scope.listaClientes.push(cliF);
        for (var k in $scope.entity.empresa.principaisClientes) {
            if ({}.hasOwnProperty.call($scope.entity.empresa.principaisClientes, k)) {
                $scope.entity.empresa.principaisClientes[k][0]='';
            }
        }
    }

    $scope.cadastrarClienteProduto = function(){
        if( !checkNotEmpty( $('.step3ValidateClientes.ng-empty:visible').eq(0) )){
            showPromptError($('.step3ValidateClientes.ng-empty:visible').eq(0), 'Este campo é obrigatório');
            return
        }

        var ccp = angular.copy($scope.entity.produto.principaisClientes)
        var ccpF = getProps(ccp);
        $scope.clientesProduto.push(ccpF);
        for (var k in $scope.entity.produto.principaisClientes) {
            if ({}.hasOwnProperty.call($scope.entity.produto.principaisClientes, k)) {
                $scope.entity.produto.principaisClientes[k][0]='';
            }
        }
    }

    $scope.removeItemFromList = function(item){
        $scope.listaEmpreendedores.splice($scope.listaEmpreendedores.indexOf(item), 1);
        $scope.entity['empreendedor']['toggleListInsert'] = $scope.listaEmpreendedores.length ? [false] : [true];
    }

    $scope.removeClassificacao = function(item){
        $scope.listaClassificacoes.splice($scope.listaClassificacoes.indexOf(item), 1);
    }

    $scope.removeCliente = function(item){
        $scope.listaClientes.splice($scope.listaClientes.indexOf(item), 1);
    }

    $scope.removeClienteProduto = function(item){
        $scope.clientesProduto.splice($scope.clientesProduto.indexOf(item), 1);
    }

    $scope.formPost = function(){

        if(!$('form[name="form_3"]').validationEngine('validate')){
                return false
        }

        var object = angular.copy($scope.entity);
        var data = getProps(object);
        var caracteristicas = Array.prototype.map.call(document.querySelectorAll('input[type=checkbox]:checked'), function(caracteristica, index) {
            return { id : caracteristica.value };
        });

        data.empreendedor = $scope.listaEmpreendedores;
        data.empresa.principaisClientes = $scope.listaClientes;
        data.produto.tags = $scope.listaClassificacoes;
        data.produto.principaisClientes = $scope.clientesProduto;
        data.produto.imagens = $scope.imagensProduto;
        data.produto.estudosDeCaso = [{
            nome: data.produto.estudoDeCaso.nome,
            imagens: $scope.documentosEstudoDeCaso
        }];
        data.produto.caracteristicas = caracteristicas;

        logger.trace('data', data);

        var request = angular.extend(API.EMPRESA(), {
            data: data
        });

        logger.trace('request', request);

        $http(request)
            .then(function(response) {
                $scope.currentStep = 4;
                logger.log(response);
            }, function(response) {
                $scope.currentStep = 5;
                logger.error(response);
            });
    }

    $scope.getCEP = function(scope, focus){
        var appFields = ['bairro','cidade','endereco','estado','pais'];
        var resFields = ['neighborhood','city','street','state','pais'];
        var scopeIndex = scope.split('.');
        var cep = angular.copy($scope.entity[scopeIndex[0]][scopeIndex[1]].cep[0]);
        if(!cep){
            return false
        }
        cep = ''+cep.replace('-', '').replace('_', '').replace(' ', '');
        if(cep.length < 8){
            return false
        }

        $http.get(CEP.ENDPOINT + cep)
            .then(function(response) {
                response.data['pais'] = 'Brasil';

                for (var i = appFields.length - 1; i >= 0; i--) {
                    $scope.entity[scopeIndex[0]][scopeIndex[1]][appFields[i]][0] = response.data[resFields[i]]
                }

                angular.element(document.getElementById(focus)).focus();
                logger.log(response);
            }, function(response) {
                logger.error(response);
            });
    }

    function onWhenAddingFileFailed(item /*{File|FileLikeObject}*/, filter, options) {
        logger.debug('onWhenAddingFileFailed', item, filter, options);

        // TODO informar sobre arquivo inválido
    };

    function onSuccessItem(fileItem, response) {
        logger.debug('onSuccessItem', fileItem, response);

        fileItem.file.entity = response;

        if (fileItem.uploader.name == 'image') {
            $scope.imagensProduto.push(response);
        } else if (fileItem.uploader.name == 'document') {
            $scope.documentosEstudoDeCaso.push(response);
        }

        // TODO informar upload
    };

    function onErrorItem(fileItem, response, status, headers) {
        logger.debug('onErrorItem', fileItem, response, status, headers);

        // TODO informar erro
    };
}
